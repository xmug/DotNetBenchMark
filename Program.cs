﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using DotNetBenchMark;

public class Program
{
    public class BenchMarkPlatform
    {
        public List<Sender> senders = new List<Sender>();
        public List<Receiver> receivers = new List<Receiver>();

        public BenchMarkPlatform()
        {
            for (int i = 0; i < 100; i++)
            {
                Sender sender = new Sender();
                senders.Add(sender);
                receivers.Add(new Receiver(sender));
            }
        }

        [Benchmark]
        public void NewRun()
        {
            for (int i = 0; i < senders.Count; i++)
            {
                Sender sender = senders[i];
                sender.Value = sender.Value > 0 ? 0 : 1;
            }
        }

        [Benchmark]
        public void OldRun()
        {
            for (int i = 0; i < senders.Count; i++)
            {
                Sender sender = senders[i];
                sender.OldValue = sender.OldValue > 0 ? 0 : 1;
            }
        }

    }

    public static void Main()
    {
        var summary = BenchmarkRunner.Run<BenchMarkPlatform>();
    }
}