﻿using UniVent;
using static DotNetBenchMark.Sender;

namespace DotNetBenchMark
{
    public class Receiver : IEventReceiver<ValueChangedEvent>, IOldValueChanged
    {
        public int value;
        public int oldValue;

        public void OnOldValueChanged(int v)
        {
            oldValue = v;
        }

        void IEventReceiver<ValueChangedEvent>.CallBack(ValueChangedEvent eventParam)
        {
            value = eventParam.v;
        }

        public Sender Sender;

        public Receiver(Sender Sender)
        {
            this.Sender = Sender;
            Sender.RegisterEvent(this);
            Sender.AddOldValueChangedCallback(this);
        }

        ~Receiver()
        {
            Sender.UnRegisterEvent(this);
            Sender.RemoveOldValueChangedCallback(this);
        }
    }
}
