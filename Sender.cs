﻿using UniVent;

namespace DotNetBenchMark
{
    public struct ValueChangedEvent : ILocalEvent
    {
        public int v;

        int ILocalEvent.EventIndex { get; set; }
    }

    public class Sender : IEventSender<ValueChangedEvent>
    {
        private static Sender _instance;
        public static Sender Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Sender();  
                return _instance;
            }
        }

        ValueChangedEvent IEventSender<ValueChangedEvent>.EventIdentification { get; set; }

        private int _value;

        public int Value
        {
            get { return _value; }
            set
            {
                if( _value != value )
                {
                    this.InvokeEvent(new ValueChangedEvent()
                    {
                        v = value
                    });
                    _value = value;
                }
            }
        }

        public interface IOldValueChanged
        {
            public void OnOldValueChanged(int v);
        }

        public List<IOldValueChanged> oldValueChangeds = new List<IOldValueChanged>(256);

        public void AddOldValueChangedCallback(IOldValueChanged oldValueChanged)
        {
            oldValueChangeds.Add(oldValueChanged);
        }

        public void RemoveOldValueChangedCallback(IOldValueChanged oldValueChanged)
        {
            oldValueChangeds.Remove(oldValueChanged);
        }

        private int _oldValue;

        public int OldValue
        {
            get { return _oldValue; }
            set
            {
                if( _oldValue != value )
                {
                    for( int i = 0; i < oldValueChangeds.Count; i++ )
                    {
                        oldValueChangeds[i].OnOldValueChanged(value);
                    }
                    _oldValue = value;
                }
            }
        }
    }
}
