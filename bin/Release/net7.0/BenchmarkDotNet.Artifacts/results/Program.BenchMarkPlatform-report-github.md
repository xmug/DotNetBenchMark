``` ini

BenchmarkDotNet=v0.13.5, OS=Windows 10 (10.0.19045.2486/22H2/2022Update)
AMD Ryzen 9 5900X, 1 CPU, 24 logical and 12 physical cores
.NET SDK=7.0.201
  [Host]     : .NET 7.0.3 (7.0.323.6910), X64 RyuJIT AVX2
  DefaultJob : .NET 7.0.3 (7.0.323.6910), X64 RyuJIT AVX2


```
| Method |       Mean |   Error |  StdDev |
|------- |-----------:|--------:|--------:|
| NewRun | 1,129.4 ns | 4.56 ns | 3.80 ns |
| OldRun |   513.0 ns | 2.58 ns | 2.41 ns |
